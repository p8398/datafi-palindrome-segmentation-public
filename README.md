# DataFi Market Segmentation

Proof of concept segmentation via the DataFi award

# Pre-requisite Libraries
- jupyter
- os

- pandas
- geopandas
- numpy
- matplotlib
- sklearn

- statsmodels
- samplics

- pytrends


# Analysis Process

The outline of the analysis process is as follows:
1. [AUTO] SA.0 outputs possibly usable variables.

2. [AUTO] SA.1 merges DHS, AHS, HIV, and GPS datasets, keeping these variables only. Output DHS_merged dataset.

3. [MANUAL] Look through these variables and pick ones relevant for analysis. Use these in the next notebook.

4. [MANUAL] SA.2 - Determine what kind of cleaning each variable needs to be usable. Clean these and output DHS_merged_processed dataset.

5. [MANUAL] SA.3 – Finalise variable list and determine how they should be coded for the tree. 

6. [AUTO] The tree is automatically generated on data that is input to it. The tree diagram is exported and each row in dataset is tagged with its allocated segment and saved to file. Outputs DHS_with_segment_IDs.

7. [MANUAL] Inspect and interpret the tree and decide which segments to group.

8. [MANUAL] SA.4 - Add “grouped segment labels” per person as new variable.

9. [AUTO] SA.4 - Summary statistics are created for every variable in the dataset for every segment and grouped segment.

10. [AUTO] SA.4 - Per-district and region summary stats are also created (e.g. n people in each segment per district, internet access in district etc.)

11. [AUTO] SA.5 – The geographic summary table is enhanced with the addition of N private and public health facilities and Google Trends searches.

12. [Manual] Analysis and interpretation of segments and their profiles (i.e. Kieran's excel sheets).

## Note

- The notebooks read from the bespoke function library "palindrome_functions.py". 

# Expected Folder structure 

```
(folders must be made before code is run)
ROOT Folder # NOTE: path to this folder to be added to top of each notebook  
├───Code  
│   # place contents of this repository here  
│   ├───Notebooks - Kenya  
│   └───Notebooks - South Africa  
│  
├───Data  
│   # Raw downloaded data here  
│   ├───DHS  
│   │   ├───KE  
│   │   │   ├───KEIR72DT    # DHS Individuals (Women)  
│   │   │   └───KEMR72DT    # DHS Men  
│   │   └───SA  
│   │       ├───ZAAH71DT    # AHS  
│   │       ├───ZAAR71DT    # HIV  
│   │       ├───ZAGE71FL    # GPS  
│   │       ├───ZAIR71DT    # DHS Individuals (Women)  
│   │       └───ZAMR71DT    # DHS Men  
│   │  
│   ├───SA Facility Types  
│   │   # Contains "vwOrgunitStructureOU5.csv" from NDOH Data Dictionary  
│   └───UNOCHA Subnational Administrative Boundaries  
│       ├───ken_adm_iebc_20191031_shp  
│       └───zaf_adm_sadb_ocha_20201109_shp  
│  
├───Output Data - SA  
│   # Where the notebooks export data, results, and figures  
│   ├───District Name Matching  
│   ├───Processed Data  
│   ├───Results  
│   │   ├───Figures  
│   │   │   └───Maps  
│   │   ├───Final Summary Stats  
│   │   └───Tree Flowchart Diagrams  
│   └───Variables  
│  
└───Output Data - KE  
   . # Where the notebooks export data, results, and figures  
   ├───Processed Data  
   ├───Results  
   │   ├───Figures  
   │   │   └───Maps  
   │   ├───Final Summary Stats  
   │   └───Tree Flowchart Diagrams  
   └───Variables  
```

# To complete:
- Clean and integrated automatic confidence interval measures into SA.4
- Update Kenya notebooks to match latest SA notebooks
- Export function documentations