############### Imports ################################
import pandas as pd
import numpy as np

# for extracting information about the tree
from sklearn import tree
# for visualising tree
import graphviz

# for Google Trends (from pypi.org/project/pytrends/)
from pytrends.request import TrendReq


############### XX.0 Variable Extraction ###############

# Functions to build a useable and cleaned variable list with descriptions
# and observation counts for any given Stata data file.
def get_variable_summaries_stata(path_to_stata_data):
    """
    Reads a Stata data file and returns a dataframe with information about its variables:
    
    variable: variable name
    description: variable description
    n: number of observations with non-missing values for this variable
    dtype: data type
    values: list of possible values for this variable
    
    Args:
      path_to_stata_data: the path to the stata survey data file you want to read in.
    
    Returns:
      A dataframe with the following columns:
        - variable: the name of the variable
        - description: the description of the variable
        - n: the number of observations with non-missing values for the variable
        - dtype: the data type of the variable
        - values: the values of the variable
    """

    ### read stata file and keep in memory
    stata_reader = pd.read_stata(path_to_stata_data, iterator=True)

    ### var_values
    # Get variable names and the list of possible values for each variable
    # i.e. the mappings from integers to text variable values (e.g. 0 is 15-19yr, 1 is 20-24yr olds)
    value_dict = stata_reader.value_labels()
    # convert variables names to lower case so it matches data.
    # also convert variable value list to string (so it can be stored in dataframe column)
    value_dict_str = {key.lower(): str(value) for key, value in value_dict.items()}
    # convert to dataframe
    var_values = pd.DataFrame.from_dict(value_dict_str, orient="index")[0]

    ### var_descs
    # extract variable descriptions as a list (these are not available in the pandas df above)
    var_descs = list(stata_reader.variable_labels().values())

    ### var_dtypes
    # get data as pandas dataframe
    data = stata_reader.read()
    # get data type for each variable
    var_dtypes = data.dtypes.tolist()

    ### make dataframe with columns for:
    # variable name
    # availability count
    # description
    # dtype
    # variable values

    var_df = pd.DataFrame(data.count())
    var_df["description"] = var_descs
    var_df["values"] = var_values
    var_df["dtype"] = var_dtypes

    # rename columns
    var_df.reset_index(inplace=True)
    var_df.rename(columns={"index": "variable", 0: "n"}, inplace=True)
    # reorder columns
    var_df = var_df[["variable", "description", "n", "dtype", "values"]]

    return var_df


def drop_unusable_variables(
    var_df,
    min_n_rows=500,
    drop_vars_very_low_n=True,
    drop_vars_na_in_desc=True,
    drop_duplicated_vars=True,
):
    """
    This function filters out unusable variables and returns two dataframes, 
    one with info on the usable vars and one dropped vars.

    1. Drop any variables that have less than min_n_rows non-null values (default 500)
    2. Drop any variables that have "na - " in their descriptions
    3. Drop any variables that are duplicated
       (e.g. are coded across multiple variables like information about multiple children)
    
    Args:
      var_df: the dataframe of variables from "get_variable_summaries_stata"
      min_n_rows: drop any vars with less than min_n_rows values. Defaults to 500.
      drop_vars_very_low_n: drop any vars with less than min_n_rows values. Defaults to True.
      drop_vars_na_in_desc: drop any vars with "na - " in their descriptions. Defaults to True.
      drop_duplicated_vars: drop any duplicated vars. Defaults to True.
    
    Returns:
      A dataframe with the usable variables and a dataframe with the dropped variables (includes extra drop_reason column).
    """

    # create df to store usable variables
    usable_vars_df = var_df.copy()

    # create df to store dropped variables
    dropped_vars_df = pd.DataFrame(
        columns=["variable", "description", "n", "dtype", "values", "drop_reason"]
    )

    if drop_vars_na_in_desc:
        #### drop rows with "na - " in their descriptions
        desc_says_na = usable_vars_df["description"].str.contains("na - ")
        print("Variables with na in description:", sum(desc_says_na))
        # store the to-be-dropped
        dropped_vars_temp = usable_vars_df[desc_says_na]
        dropped_vars_temp = dropped_vars_temp.assign(drop_reason="desc_says_na")
        dropped_vars_df = dropped_vars_df.append(dropped_vars_temp)
        # remove from list
        usable_vars_df = usable_vars_df[~desc_says_na]

    if drop_vars_very_low_n:
        #### drop any vars with less than min_n_rows values
        not_enough_values = usable_vars_df["n"] < min_n_rows
        print(
            "Variables with less than {} values:".format(min_n_rows),
            sum(not_enough_values),
        )
        # store the to-be-dropped
        dropped_vars_temp = usable_vars_df[not_enough_values]
        dropped_vars_temp = dropped_vars_temp.assign(drop_reason="not_enough_values")
        dropped_vars_df = dropped_vars_df.append(dropped_vars_temp)
        # remove from list
        usable_vars_df = usable_vars_df[~not_enough_values]

    if drop_duplicated_vars:
        #### remove duplicate variables
        # (in this analysis, we don't need any of the variables that are coded across multiple variables e.g. information about each child)

        # TODO: # sort by descending availability so that the most available version of a duplicated variable is kept
        # usable_vars_df.sort_values(by="n", ascending=False, inplace=True)

        # mark duplicated vars (keeps first instance)
        duplicated_vars = usable_vars_df["description"].duplicated(
            keep="first"
        )  # first : Mark duplicates as True except for the first occurrence.
        print("Variables with duplicate descriptions:", sum(duplicated_vars))
        # store the to-be-dropped
        dropped_vars_temp = usable_vars_df[duplicated_vars]
        dropped_vars_temp = dropped_vars_temp.assign(drop_reason="duplicated_desc")
        dropped_vars_df = dropped_vars_df.append(dropped_vars_temp)
        # remove from list
        usable_vars_df = usable_vars_df[
            ~duplicated_vars
        ]  # usable_vars_df.drop_duplicates("description")

        # TODO: # sort by index again to get correct order
        # usable_vars_df.sort_index(ascending=True, inplace=True)
        # dropped_vars_df.sort_index(ascending=True, inplace=True)

    return usable_vars_df, dropped_vars_df


def get_stata_variable_info(
    path_to_stata_data,
    min_n_rows=500,
    drop_vars_very_low_n=True,
    drop_vars_na_in_desc=True,
    drop_duplicated_vars=True,
):
    """
    Get dataframes of the usable and unusable (dropped) variables in a Stata data file.
    Each dataframe contains the name, description, availability, type, and possible values of each variable.
    The dataframe for the dropped variables also includes the reason for dropping.
    
    Args:
      path_to_stata_data: path to the stata data file
      min_n_rows: drop any vars with less than min_n_rows values. Defaults to 500.
      drop_vars_very_low_n: drop any vars with less than min_n_rows values. Defaults to True.
      drop_vars_na_in_desc: drop any vars with "na - " in their descriptions. Defaults to True.
      drop_duplicated_vars: drop any duplicated vars. Defaults to True.
    
    Returns:
      A dataframe with the usable variables with the following columns
        - variable: the name of the variable
        - description: the description of the variable
        - n: the number of observations with non-missing values for the variable
        - dtype: the data type of the variable
        - values: the values of the variable
      And another dataframe with the dropped variables with the above columns and an extra drop_reason column.
    """

    # get info for all variables
    var_df = get_variable_summaries_stata(path_to_stata_data)
    # filter by usability
    usable_vars_df, dropped_vars_df = drop_unusable_variables(
        var_df,
        min_n_rows,
        drop_vars_very_low_n,
        drop_vars_na_in_desc,
        drop_duplicated_vars,
    )

    print("Total usable variables:", usable_vars_df.shape[0])

    return usable_vars_df, dropped_vars_df


############### XX.3 Modelling #########################

def export_tree_diagram(
    # graphviz params
    decision_tree,
    feature_names,
    rates=True,
    format="png",
    # filesaving params
    path_to_folder="",
    file_prefix="tree_flowchart",
):
    """
    This function creates a flowchart of the input decision tree.
    
    Args:
      decision_tree: the decision tree model object to be drawn.
      feature_names: list of feature names (str) that the tree expects to see.
      rates: True to display rates, False to display counts. Defaults to True.
      format: The output format of the graph, png or pdf. Defaults to png.
      path_to_folder: the path to the folder where the tree diagram will be saved.
      file_prefix: the prefix of the name of the file to be saved. Defaults to "tree_flowchart".
    """

    dot_data = tree.export_graphviz(
        # tree and feature names
        decision_tree=decision_tree,
        feature_names=feature_names,
        # counts or rates displayed
        proportion=(True if rates else False),
        precision=(3 if rates else 1),
        # other display parameters
        label="root",
        impurity=False,
        filled=True,
        rounded=True,
        special_characters=True,
    )

    graph = graphviz.Source(dot_data)
    graph.render(
        filename=path_to_folder + file_prefix + ("_rates" if rates else "_counts"),
        format=format,
        cleanup=True,
    )


def get_tree_code(decision_tree, feature_names):
    """
    This function converts a Scikit-learn decision tree to Python code. That code can then take in a person's 
    feature values and return the id of the leaf (i.e. "segment") that that person untimately falls into.
    
    Args:
      decision_tree: the decision tree model object that we created earlier
      feature_names: list of feature names (str) that the tree expects to see
    
    Returns:
      The function that we can use to get the leaf node allocation for each respondant, as a string.
    """

    tree_ = decision_tree.tree_
    feature_name = [
        feature_names[i] if i != tree._tree.TREE_UNDEFINED else "undefined!"
        for i in tree_.feature
    ]

    # setup vars
    global leaf_id
    global tree_code
    # set first leaf_id
    leaf_id = 0

    # initialise Python code string
    tree_code = "def get_leaf_node_id(X_row):"
    tree_code += "\n  {} = X_row".format(", ".join(feature_names))
    tree_code += "\n"

    def recurse(node, depth):
        # make these local
        global leaf_id
        global tree_code
        indent = "  " * depth

        if tree_.feature[node] != tree._tree.TREE_UNDEFINED:
            name = feature_name[node]
            threshold = tree_.threshold[node]
            tree_code += "\n{}if {} <= {}:".format(indent, name, threshold)
            recurse(tree_.children_left[node], depth + 1)
            tree_code += "\n{}else:  # if {} > {}".format(indent, name, threshold)
            recurse(tree_.children_right[node], depth + 1)
        else:
            tree_code += "\n{}return {}".format(indent, leaf_id)
            leaf_id += 1

    recurse(0, 1)

    return tree_code


# function to get segment (leaf nodes) summaries with rules included. Legacy Code.
def get_leaf_nodes_info(decision_tree, feature_names, class_names):
    """
    The function* takes a decision tree and returns a dataframe summarising the values of the 
    target variable in leaf nodes of the tree (the "segments") with the following columns:
    *based on stackoverflow.com/questions/30408046/

    condition: the condition that splits the data into the leaf node
    n_public: the number of people who've only used public healthcare that fall into this leaf node
    n_private: the number of people who have used healthcare that fall into this leaf node
    n_total: the total number of people that fall into this leaf node
    
    Args:
      decision_tree: the decision tree.
      feature_names: list of feature names.
      class_names: a list of class names, ordered according to whatever the classifier was using.
    
    Returns:
      a dataframe with the following columns:
        condition - the condition that splits the data into the leaf node
        n_public - the number of people who've only used public healthcare that fall into this leaf node
        n_private - the number of people who have used healthcare that fall into this leaf node
        n_total - the total number of people that fall into this leaf node
    """
    
    tree_ = decision_tree.tree_
    feature_name = [
        feature_names[i] if i != tree._tree.TREE_UNDEFINED else "undefined!"
        for i in tree_.feature
    ]

    paths = []
    path = []

    def recurse(node, path, paths):
        if tree_.feature[node] != tree._tree.TREE_UNDEFINED:
            name = feature_name[node]
            threshold = tree_.threshold[node]
            p1, p2 = list(path), list(path)
            p1 += [f"({name} <= {np.round(threshold, 3)})"]
            recurse(tree_.children_left[node], p1, paths)
            p2 += [f"({name} > {np.round(threshold, 3)})"]
            recurse(tree_.children_right[node], p2, paths)
        else:
            path += [(tree_.value[node], tree_.n_node_samples[node])]
            paths += [path]

    recurse(0, path, paths)

    rows = []
    for path in paths:

        row = []
        condition = "if "

        for p in path[:-1]:
            if condition != "if ":
                condition += " and "
            condition += str(p)

        row.append(condition)

        if class_names is None:
            raise Exception("class_names not provided!")
            # condition += "response: "+str(np.round(path[-1][0][0][0],3))
        else:
            classes = path[-1][0][0]
            row.append(classes[0])
            row.append(classes[1])

        rows.append(row)

    segments_df = pd.DataFrame(rows, columns=["condition", "n_public", "n_private"])
    segments_df["n_total"] = segments_df["n_public"] + segments_df["n_private"]

    return segments_df


############### XX.4 Segment Stats #####################

def get_weighted_grouped_stats(
    df,
    value_count_var,
    groupby_var,
    stat_type="ratio_vs_baseline",
    prefix="",
    sample_weight="dhs_sample_weight"
):
    """
    Function for getting weighted grouped stats for a single variable (i.e. weighted cross tabulation).

    The function takes in a dataframe, the variable to summarise on, the variable to group by, 
    and the type of summary stats you want. It returns a dataframe with information about the values 
    of the variable of interest per values of the variable to group by (e.g. segment or district).
    
    The stat_type can be either "ratio_vs_baseline", "segment_counts", "segment_percentages", 
    "country_counts", or "country_percentages". 
    
    The prefix is the prefix of the column names of the returned dataframe.
    
    Args:
      df: the dataframe to be used
      value_count_var: the variable that you want to summarise
      groupby_var: the variable to group by
      stat_type: ratio_vs_baseline, segment_counts, segment_percentages, country_counts,
    country_percentages. Defaults to ratio_vs_baseline
      prefix: the prefix for the new column names
      sample_weight: the name of the column that contains the sample weights. Defaults to
    dhs_sample_weight
    
    Returns:
      dataframe: summary stats table
    """

    #### PER SEGMENT
    # get grouped values
    weighted_value_counts = (
        df.groupby([groupby_var, value_count_var])[sample_weight].sum().sort_index()
    )
    # get total counts
    weighted_totals = weighted_value_counts.groupby(groupby_var).sum()
    # get prevalence per segment
    segment_percentages = (weighted_value_counts / weighted_totals) * 100

    ### WHOLE COUNTRY BASELINE
    groupby_var_country = "country"
    # get SA grouped values
    country_weighted_value_counts = (
        df.groupby([groupby_var_country, value_count_var])[sample_weight]
        .sum()
        .sort_index()
    )
    # get total country counts
    country_weighted_totals = country_weighted_value_counts.groupby(groupby_var_country).sum()
    # get prevalence in country
    country_percentages = (country_weighted_value_counts / country_weighted_totals) * 100

    if stat_type == "ratio_vs_baseline":
        # devation from/multiple of the country mean (segment_vs_national_ratio)
        result = segment_percentages / country_percentages

    elif stat_type == "segment_counts":
        result = weighted_value_counts

    elif stat_type == "segment_percentages":
        result = segment_percentages

    elif stat_type == "country_counts":
        result = country_weighted_value_counts
        groupby_var = groupby_var_country

    elif stat_type == "country_percentages":
        result = country_percentages
        groupby_var = groupby_var_country

    else:
        raise ValueError("stat_type not valid!")

    # convert to df
    df_result = result.to_frame().round(2)
    df_result.columns = ["count"]
    df_result = df_result.reset_index()

    # add prefix
    if prefix == "":
        prefix = value_count_var

    # pivot to get correct dataframe shape
    df_result = df_result.pivot(groupby_var, value_count_var, "count").add_prefix(
        prefix + "_"
    )

    return df_result



############### XX.5 Enhanced Geo Stats ################

# Function to retrieve data from Google Trends
def get_google_trends_data(search_texts, search_labels=None):
    """
    For a given search term, this function returns a dataframe of the associated Google Trends
    time series data, a dataframe of the regional data, and a dataframe of the metro data
    
    Args:
      search_text: The search term to be used in the API call.
      search_label: the label for the search term
    
    Returns:
      a tuple of four objects:
        1. The pytrends object
        2. time_df dataframe
        3. region_df dataframe
        4. metro_df dataframe
    """

    # if search_labels is None:
    #     search_labels = ["search_for_"+search_text.replace(" ", "_") for search_text in search_texts]
    # relabel_dict = dict(zip(search_texts, search_labels))
    
    # build query
    pytrends = TrendReq(
        hl='en-US', 
        tz=120 # timezone: CAT in minutes
        )

    # build payload
    kw_list = search_texts # list of keywords to get data 
    pytrends.build_payload(kw_list, cat=0, geo='ZA', timeframe='today 5-y')

    # interest over time
    time_df = pytrends.interest_over_time() 
    time_df = time_df[~time_df["isPartial"]].drop("isPartial", axis=1) #.iloc[:,0]#.rename(search_label)

    # interest by region
    region_df = pytrends.interest_by_region(resolution='REGION', inc_low_vol=True, inc_geo_code=False)
    region_df = region_df.reset_index().rename({'geoName':'region'}, axis=1) #**relabel_dict

    # interest by metro area
    metro_df = pytrends.interest_by_region(resolution='DMA', inc_low_vol=True, inc_geo_code=False)
    metro_df = metro_df.reset_index().rename({'geoName':'metro'}, axis=1) #**relabel_dict

    return pytrends, time_df, region_df, metro_df